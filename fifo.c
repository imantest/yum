#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

//linked list struct
// typedef struct listnode2
// {
//    unsigned int framenumber;
//    struct listnode2 *next;
//
// }listnodeT;

//first node of linked list
listnodeT *frame_list;

/* Page to evict is chosen using the fifo algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int fifo_evict() {

  // evicting head of linked list
	int framenumber = (int) frame_list->framenumber;
	listnodeT *temp;
	temp = frame_list;
	frame_list = frame_list->next;
	free(temp);
	coremap[framenumber].ptoll = NULL;

	return framenumber;
}

/* This function is called on each access to a page to update any information
 * needed by the fifo algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void fifo_ref(pgtbl_entry_t *p) {
	unsigned framenumber = (p->frame) >> PAGE_SHIFT;
	if (coremap[framenumber].ptoll == NULL) {
			listnodeT *nextelement = malloc(sizeof(listnodeT));
			nextelement->framenumber = framenumber;
			nextelement->next = NULL;
			coremap[framenumber].ptoll = nextelement;
		  //case when linked list is empty @@@@@@@@@@Usman what happens to repeat elemnts inside ll
			if(frame_list == NULL){
				frame_list = nextelement;
				frame_list->next = NULL;
		  //case when linked list has an element
			}else{
				frame_list->next = nextelement;
			}
	}
	return;
}

/* Initialize any data structures needed for this
 * replacement algorithm
 */
void fifo_init() {
  //intializing the linked list
	frame_list = malloc(sizeof(listnodeT));
	frame_list->next = NULL;
}
