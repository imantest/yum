#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"
#define MAXLINE 256

extern int memsize;

extern int debug;
//tracefile file
extern char *tracefile;
//array table of integers
int *reference;

//tracefile file to be opened
FILE *tfp ;
extern char* physmem;
extern struct frame *coremap;

typedef struct listnode1
{
   unsigned int vaddr;
	 struct listnode1 *next;

}listnodeW;
//line number tracker using pointer to linked list
listnodeW *filemirror;
listnodeW *endfilemirror;

/* Page to evict is chosen using the optimal (aka MIN) algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */
int opt_evict() {
	puts("3.0");
	addr_t vaddr = 0;
	addr_t *check;
	int count1 = 0;
  listnodeW *counter = filemirror;
	while(counter!=NULL) {
			vaddr = counter->vaddr;
			for(int i = 0; i < memsize; i++){
				if(vaddr == coremap[i].vaddr){
					reference[i] = 1;
					count1 ++;
          break;
				}
			}

			if(count1 == memsize -1){
				break;
			}


		counter= counter -> next;
	}

	int ret;


	for(int i = 0; i < memsize; i++){
		if(reference[i] == 0){
			ret = i;
			break;
		}
	}

	for(int i = 0; i < memsize; i++){
		reference[i] = 0;
	}

	return ret;
	puts("3.1");
}

/* This function is called on each access to a page to update any information
 * needed by the opt algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void opt_ref(pgtbl_entry_t *p) {
	puts("2.0");
	listnodeW *temp = filemirror;
	 filemirror = filemirror -> next;
	 free(temp);
	puts("2.1");
}

/* Initializes any data structures needed for this
 * replacement algorithm.
 */
void opt_init() {

	puts("ayyyyyy 1.0");

	filemirror = malloc(sizeof(listnodeW));
	filemirror->vaddr = -1;
	filemirror->next = NULL;
	endfilemirror = filemirror;
	char type;
	char lines[256];
	addr_t vaddr = 0;
	if(tracefile != NULL) {
		if((tfp = fopen(tracefile, "r")) == NULL) {
			perror("Error opening tracefile:");
			exit(1);
		}
	}

	while(fgets(lines, MAXLINE, tfp) != NULL) {
		if(lines[0] != '=') {
			sscanf(lines, "%c %lx", &type, &vaddr);
			if(endfilemirror->vaddr == -1){
				filemirror->vaddr = vaddr;
			}else{
				listnodeW *temp = malloc(sizeof(listnodeW));
				temp->vaddr = vaddr;
				endfilemirror->next = temp;
				endfilemirror = temp;
				endfilemirror->next = NULL;
			}
	}

}
	reference = malloc((sizeof(int))*memsize);
	for(int i = 0; i < memsize; i++){
		reference[i] = 0;
	}


	puts("ayyyyyy 1.1");


}
