#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;

//double linked list typedef
typedef struct listnode1
{
   unsigned int framenumber;
   struct listnode1 *next;
	 struct listnode1 *last;

}listnodeW;

//array of struct pointes
listnodeW **reference;

//head node of linked list
listnodeW *frame_list;
//tail node of linked list
listnodeW *frame_list_last;

/* Page to evict is chosen using the accurate LRU algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */

int lru_evict() {
	listnodeW *temp =  frame_list;
	int framenumber =  frame_list->framenumber;

  //if there is only one node
	if(frame_list == frame_list_last){
		frame_list = NULL;
		frame_list_last = NULL;
	}
  //the head of linked list is the LRU and is removed
	else{
	frame_list = frame_list->next;
	frame_list->last = NULL;
}
	free(temp);
	reference[framenumber] = NULL;
	return framenumber;
}

/* This function is called on each access to a page to update any information
 * needed by the lru algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void lru_ref(pgtbl_entry_t *p) {
	int framenumber = (int) ((p->frame) >> PAGE_SHIFT);
	listnodeW *temp;

  //if frame number has not been referenced yet
	if(reference[framenumber] == NULL){
		temp = malloc(sizeof(listnodeW));
		reference[framenumber] = temp;
		temp->framenumber = framenumber;
    //if there are no elemnts in the linked list
		if(frame_list->framenumber == -1){
			frame_list = temp;
			frame_list->next = NULL;
			frame_list->last = NULL;
			frame_list_last = frame_list;
		}
    //if elements exist in the linked list
		else{
		frame_list_last->next = temp;
		temp->last = frame_list_last;
		frame_list_last = temp;
		frame_list_last->next = NULL;
	}
	}
  //if frame number has been referenced
	else{
		temp = reference[framenumber];
    //if there are more than 3 elemetns in the linked list
	  if(temp->last != NULL && temp->next != NULL){
      //maintaing double linked list
			listnodeW *before = temp->last;
			listnodeW *after = temp->next;
			before->next = after;
			after->last = before;
			frame_list_last->next = temp;
			temp->last = frame_list_last;
			frame_list_last = temp;
			frame_list_last->next = NULL;
		}
    //if node is not already the last place
		else if(temp->next != NULL){
			frame_list = frame_list->next;
			frame_list->last = NULL;
			frame_list_last->next = temp;
			temp->last = frame_list_last;
			frame_list_last = temp;
			frame_list_last->next = NULL;

	}




	return;
}}


/* Initialize any data structures needed for this
 * replacement algorithm
 */
void lru_init() {
  //intilaising double linked list
	frame_list = malloc(sizeof(listnodeW));
	frame_list_last = frame_list;
	frame_list->framenumber = -1;
	frame_list->next = NULL;
	frame_list->last = NULL;

  //initializing array of struct pointers
	reference = malloc(sizeof(listnodeW*)*memsize);
	for(int i = 0; i < memsize; i++){
		reference[i] = NULL;
	}
}
