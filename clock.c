#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include "pagetable.h"


extern int memsize;

extern int debug;

extern struct frame *coremap;



//first node of linked list
listnodeT *frame_list;
//last node of linked list
listnodeT *frame_list_last;

/* Page to evict is chosen using the clock algorithm.
 * Returns the page frame number (which is also the index in the coremap)
 * for the page that is to be evicted.
 */

int clock_evict() {
	listnodeT *temp = frame_list;
	//loop through circular linked list
	while(temp->next!=NULL){
		//case if the current node is to be evicted
		if(temp->referencebit == 0){
			//case if there is onyl one node
			if(temp->next == temp){
				int framenum = temp->framenumber;
				frame_list = NULL;
				coremap[framenum].ptoll = NULL;

				free(temp);
				return framenum;
			}
			//case if there are 2 nodes
			else if(temp->next == temp->last){
				int framenum = temp->framenumber;
				frame_list = frame_list->next;
				coremap[framenum].ptoll = NULL;

				free(temp);
				return framenum;
	 		}
			//case if there are more than 2 nodes
			else{
				int framenum = temp->framenumber;
				listnodeT *before = temp->last;
				listnodeT *after = temp->next;
				before->next = after;
				after->last = before;
				//case if current node is the first node
				if(temp == frame_list){
					frame_list = after;
				}
				//case if current node is last node
				if(temp == frame_list_last){
					frame_list_last = before;
				}
				coremap[framenum].ptoll = NULL;
				free(temp);
				return framenum;
			}
		}
		//current node is not to be evicted because of reference bit
		else if (temp->referencebit == 1){
			temp->referencebit = 0;
		}
		temp = temp->next;
	}
return 0;
}
/* This function is called on each access to a page to update any information
 * needed by the clock algorithm.
 * Input: The page table entry for the page that is being accessed.
 */
void clock_ref(pgtbl_entry_t *p) {
	int framenumber = (int) ((p->frame) >> PAGE_SHIFT);
	listnodeT *temp;

	//if this frame number has not been accesed before
	if(coremap[framenumber].ptoll == NULL){
		//creates pointer to node in coremap
		temp = malloc(sizeof(listnodeT));
		coremap[framenumber].ptoll = temp;
		temp->framenumber = framenumber;
		temp->referencebit = 1;
		//if linked list has never been accessed before
		if(frame_list->framenumber == -1){
			//inserting node
			frame_list = temp;
			frame_list->next = frame_list;
			frame_list->last = frame_list;
			//maintaing circular linked list
			frame_list_last = frame_list;
		}
		//if linked list has nodes inside
		else{
		//inserting node
		frame_list_last->next = temp;
		temp->last = frame_list_last;
		frame_list_last = temp;
		frame_list_last->next = frame_list;
		//maintating circular linked list
		frame_list->last = frame_list_last;

	}
	}
	else{
		//if pointer does exist so therefore this is a simple reference
	  if ((coremap[framenumber].ptoll)->referencebit == 0){
			(coremap[framenumber].ptoll)->referencebit = 1;
		}
}
}


/* Initialize any data structures needed for this replacement
 * algorithm.
 */
void clock_init() {
	//initializing linked list
	frame_list = malloc(sizeof(listnodeT));
	frame_list_last = frame_list;
	//setting frame number to -1 to show if it has not been accessed before
	frame_list->framenumber = -1;
	frame_list->next = NULL;
	frame_list->last = NULL;
}
